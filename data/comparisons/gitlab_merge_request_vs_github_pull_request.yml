title: "GitHub Pull Request vs GitLab Merge Request"
#pdf: gitlab-merge-request-vs-github-pull-request.pdf
pdf: 'null'
competitor_two:
  name: 'GitHub Pull Requests'
  logo: '/images/comparison/github-logo.svg'
competitor_one:
  name: 'GitLab Merge Request'
  logo: '/images/comparison/gitlab-logo.svg'
last_updated: 'May 16, 2017'
features:
  - title: "Labels"
    description: "Labels provide an easy way to categorize issues or merge requests based on descriptive titles as 'bug', or 'documentation'."
    competitor_one: true
    competitor_two: true
    link: https://docs.gitlab.com/ce/user/project/labels.html
    link_description: "Learn more about GitLab Labels."
  - title: "Improved UX"
    description: "GitLab Merge Requests have their own description and title displayed before the timeline feed."
    competitor_one: true
    competitor_two: sortof
    link: '/images/comparison/github-pull-request-vs-gitlab-merge-request.png'
    link_description: |
        <i class="fa fa-file-image-o" aria-hidden="true" style="font-size: .9em"></i> View side-by-side comparison
  - title: "Edit merge request"
    description: "GitLab Merge Requests are editable by the author, the project's owners and users with master access. Every field is editable, as well as the target branch."
    competitor_one: true
    competitor_two: false
    link: 'https://docs.gitlab.com/ee/user/project/merge_requests/'
    link_description: Read through GitLab Merge Request's Documentation.
  - title: "Built-in Continuous Integration"
    description: "Use GitLab CI/CD (built-in GitLab) to build, test, and deploy your app with continuous methods, and your build results will be displayed on merge requests."
    competitor_one: true
    competitor_two: false
    link: 'https://docs.gitlab.com/ee/ci/pipelines.html#pipeline-graphs'
    link_description: Take a peek at GitLab's Pipeline Graphs.
  - title: "Fast-Forward merge available for all merge requests"
    description: |
      With fast-forward merges, you keep a linear Git history without creating merge commits. With GitLab, they're configurable on a per-project basis; with GitHub, they are available only for squashed or rebased merge commits.
    competitor_one: true
    competitor_two: false
    link: 'https://docs.gitlab.com/ee/user/project/merge_requests/fast_forward_merge.html#fast-forward-merge-requests'
    link_description: "Lean more about Fast-Forward Merge Requests."
  - title: "Fix merge conflicts from the UI"
    description: "When a merge request has conflicts, it is possible to resolve those conflicts right from the GitLab UI."
    link_description: "Learn more about Merge Conflict Resolution."
    link: "https://docs.gitlab.com/ee/user/project/merge_requests/resolve_conflicts.html#merge-conflict-resolution"
    competitor_one: true
    competitor_two: true
  - title: "Close issue(s) when merged"
    description: "With GitLab, you can use specific keywords to close one or more issues as soon as a merge request is merged."
    competitor_one: true
    competitor_two: true
    link_description: "Learn more about automatically closing issues."
    link: "https://docs.gitlab.com/ce/user/project/issues/closing_issues.html#via-merge-request"
  - title: "Review Apps"
    description: |
      With  <a href="/features/review-apps/">GitLab Review Apps</a>, you can preview the changes made to your app by that merge request in dynamic environments. These environments are automaticaly destroyed when the merge request gets merged.
    competitor_one: true
    competitor_two: false
    link: 'https://docs.gitlab.com/ce/ci/review_apps/index.html#getting-started-with-review-apps'
    link_description: Learn how to get started with Review Apps.
  - title: "Integrated web terminal"
    description: "GitLab can open a terminal session directly from your environment if your review app is deployed on Kubernetes. This is a very powerful feature where you can quickly debug issues without leaving the comfort of your web browser."
    competitor_one: true
    competitor_two: false
    link: 'https://docs.gitlab.com/ce/ci/environments.html#web-terminals'
    link_description: "Learn more about GitLab Web Terminals."
  - title: "Watch your jobs running"
    description: |
      With <a href="/features/gitlab-ci-cd">GitLab's built-in CI/CD</a>, you can watch your jobs running right from the UI for debugging and optimizing your CI scripts.
    competitor_one: true
    competitor_two: false
  - title: "Squash and merge"
    description: "Squashing lets you tidy up the commit history of a branch when accepting a merge request by squashing a long list of commits into a single commit on merge."
    competitor_one: true
    competitor_two: true
    link: 'https://docs.gitlab.com/ee/user/project/merge_requests/squash_and_merge.html'
    link_description: "Learn more about GitLab Squash and Merge."
  - title: "Approvals"
    description: "Merge Request Approvals make your workflow smoother when your are working with your team. With GitLab, approvals are configured per project, and a user can choose the approver in a per-merge request basis."
    competitor_one: true
    competitor_two: true
    link: 'https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html#merge-request-approvals'
    link_description: "Read through Merge Request Approvals Documentation."
  - title: "Resolvable discussions"
    description: "Code or text review is faster and more effective with inline comments in merge requests. In GitLab, Merge Request inline comments are interpreted as a discussion. You can configure your project to only accept merge requests when all discussions are resolved."
    competitor_one: true
    competitor_two: false
    link: 'https://docs.gitlab.com/ce/user/discussions/#resolvable-discussions'
    link_description: "Learn more about Resolvable Discussions."
  - title: "Todos"
    description: "When a user is mentioned in or assigned to a merge request it will be included in the user Todos, making the development workflow faster and easier to track."
    competitor_one: true
    competitor_two: false
    link: 'https://docs.gitlab.com/ee/workflow/todos.html#gitlab-todos'
    link_description: "Learn more about GitLab Todos."
  - title: "Time tracking"
    description: "Time Tracking allows you to track estimates and time spent on issues and merge requests within GitLab."
    competitor_one: true
    competitor_two: false
    link: 'https://docs.gitlab.com/ce/workflow/time_tracking.html#time-tracking'
    link_description: "Learn more about GitLab Time Tracking."
  - title: "Integrated with Cycle Analytics"
    description: "Cycle Analytics lets you know how long it takes your team to complete each stage in their workflow from idea to production."
    competitor_one: true
    competitor_two: false
    link_description: "Learn more about GitLab Cycle Analytics."
    link: '/features/cycle-analytics/'
  - title: "Enriched markdown support"
    description: "Both Merge Request descriptions and comments support GFM - GitLab Flavored Markdown."
    competitor_one: true
    competitor_two: true
    link_description: "Learn more about GitLab Flavored Markdown."
    link: 'https://docs.gitlab.com/ce/user/markdown.html'
  - title: "Pipeline Views"
    description: "GitLab Merge Requests are presented with multiple views: Discussion, Commits, Pipelines, Changes (diffs). On GitHub, even if a repository integrates with a Continuous Integration third-party tool, you can't see its pipelines through GitHub's UI."
    competitor_one: true
    competitor_two: false
  - title: "Cherry Pick merge request"
    description: "Cherry-pick any commit in the UI by simply clicking the Cherry-Pick button in a merged merge request or a commit."
    competitor_one: true
    competitor_two: false
    link: 'https://docs.gitlab.com/ce/user/project/merge_requests/#cherry-pick-changes'
    link_description: "Learn more about Cherry Picking merge requests."
  - title: "Merge when pipeline succeeds"
    description: "When reviewing a merge request that looks ready to merge but still has one or more CI jobs running, you can set it to be merged automatically when the jobs pipeline succeeds."
    competitor_one: true
    competitor_two: false
    link: 'https://docs.gitlab.com/ce/user/project/merge_requests/merge_when_pipeline_succeeds.html'
    link_description: "Learn more about Merge when pipeline succeeds."
  - title: "Revert commits from the UI"
    description: "Revert any commit from GitLab's UI, with a click of a button."
    competitor_one: true
    competitor_two: true
    link: 'https://docs.gitlab.com/ee/user/project/merge_requests/revert_changes.html#reverting-a-commit'
    link_description: "Learn how to revert a commit or a merge request from the GitLab UI."
  - title: "Work in Progress Merge Request (WIP)"
    description: "Prevent merge requests from accidentally being accepted before they're completely ready by marking them as Work In Progress (WIP)."
    competitor_one: true
    competitor_two: false
    link: 'https://docs.gitlab.com/ce/user/project/merge_requests/work_in_progress_merge_requests.html'
    link_description: "Learn more about WIP MRs."
  - title: "Merge Request template"
    description: "By adding a description template to your MRs, users who create a new issue or merge request can select a template to help them to communicate effectively."
    competitor_one: true
    competitor_two: false
    link: 'https://docs.gitlab.com/ce/user/project/description_templates.html#creating-merge-request-templates'
    link_description: "Learn more about GitLab Description Templates."

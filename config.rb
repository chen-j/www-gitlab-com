require 'generators/direction'
require 'generators/release_list'
require 'generators/org_chart'
require 'extensions/breadcrumbs'
require 'lib/homepage'
require "lib/custom_helpers"
helpers CustomHelpers
include CustomHelpers

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

set :haml, {
  ugly: true,
  format: :xhtml
}

activate :syntax, line_numbers: false

set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: "GFM"

activate :blog do |blog|
  blog.sources = "posts/{year}-{month}-{day}-{title}.html"
  blog.permalink = "{year}/{month}/{day}/{title}/index.html"
  blog.layout = "post"
  # Allow draft posts to appear on all branches except master (for Review Apps)
  blog.publish_future_dated = true if ENV['CI_BUILD_REF_NAME'].to_s != 'master'

  blog.summary_separator = /<!--\s*more\s*-->/

  blog.custom_collections = {
    categories: {
      link: '/blog/categories/{categories}/index.html',
      template: '/category.html'
    }
  }
end

activate :autoprefixer do |config|
  config.browsers = ['last 2 versions', 'Explorer >= 9']
end

activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false

# Reload the browser automatically whenever files change
unless ENV['ENABLE_LIVERELOAD'] != '1'
  configure :development do
    activate :livereload
  end
end

# Build-specific configuration
configure :build do
  set :build_dir, "public"
  activate :minify_css
  activate :minify_javascript
  activate :minify_html

  # Populate the direction and release list pages only on master.
  # That will help shave off some time of the build times on branches.
  if ENV['CI_BUILD_REF_NAME'] == 'master' || !ENV.key('CI_BUILD_REF_NAME')
    ## Direction page
    if PRIVATE_TOKEN
      proxy "/direction/index.html", "/direction/template.html", locals: { direction: generate_direction, wishlist: generate_wishlist }, ignore: true
    end

    ## Release list page
    releases = ReleaseList.new
    proxy "/release-list/index.html", "/release-list/template.html", locals: { list: releases.generate }, ignore: true
  end

end

org_chart = OrgChart.new
proxy "/team/structure/index.html", "/team/structure/template.html", locals: { team_data_tree: org_chart.team_data_tree }, ignore: true

# Proxy comparison PDF pages
data.comparisons.each do |key, comparison|
  file_name = key.dup.gsub(/_/, '-')
  proxy "/comparison/pdfs/#{file_name}.html", "/comparison/pdfs/template.html", locals: { comparison_block: comparison }, ignore: true
end

# GitLab Projects
proxy '/handbook/engineering/projects/index.html',
      '/handbook/engineering/projects/template.html',
      locals: { team: Gitlab::Homepage::Team.new },
      ignore: true

# Proxy vacancy pages
data.vacancies.each do |opening|
  file_name = vacancy_name(opening).parameterize
  proxy "/careers/#{file_name}/index.html", "/careers/template.html", locals: { vacancy: opening }, ignore: true
end

page '/404.html', directory_index: false

ignore '/direction/template.html'
ignore '/includes/*'
ignore '/release-list/template.html'
ignore '/team/structure/org-chart/template.html'
ignore '/source/stylesheets/highlight.css'
ignore '/category.html'

---
layout: markdown_page
title: "Benefits"
---


## On this page
{:.no_toc}

- TOC
{:toc}

----


NOTE: Our contractor agreements and employment contracts are all on the [Contracts](https://about.gitlab.com/handbook/contracts/) page.



## General Benefits

1. GitLab will pay for the items listed under [spending company money](https://about.gitlab.com/handbook/spending-company-money).
1. [Stock options](/handbook/stock-options/) are offered to most GitLabbers.
1.  Deceased team member:
    In the unfortunate event that a GitLab team member passes away, GitLab will
    provide a $20,000 lump sum to anyone of their choosing. This can be a spouse,
    partner, family member, friend, or charity.
      * For US based employees of GitLab Inc., this benefit is replaced by the
        [Basic Life Insurance](#basic-life-ins) that is offered through TriNet.
      * For all other GitLabbers, the following conditions apply:
         * The team member must be either an employee or direct contractor,
         * The team member must have indicated in writing to whom the money
           should be transferred.
         * For part-time GitLabbers, the lump sum is calculated pro-rata, so
           for example for a team member that works for GitLab 50% of the time,
           the lump sum would be $10,000.
1. [Paid time off policy](https://about.gitlab.com/handbook/paid-time-off).
1. [GitLab Summit](https://about.gitlab.com/culture/summits)
   * Every nine months or so GitLabbers gather at an exciting new location to [stay connected](https://about.gitlab.com/2016/12/05/how-we-stay-connected-as-a-remote-company/), at what we like to call a GitLab Summit. It is important to spend time face to face to get to know your team and, if possible, meet everyone who has also [bought into the company vision](http://www.excitingrole.com/how-to-do-startup-due-diligence/#.h/). There are fun activities planned by our GitLab Summit Experts, work time, and presentations from different functional groups to make this an experience that you are unlikely to forget! Attendance is optional, but encouraged. For more information and compilations of our past summits check out our [summits page](https://about.gitlab.com/culture/summits).
1. [Business Travel Accident Policy](https://drive.google.com/a/gitlab.com/file/d/0B4eFM43gu7VPVl9rYW4tXzIyeUlMR0hidWIzNk1sZjJyLUhB/view?usp=sharing)
   *  This policy provides coverage for team members who travel domestic and internationally for business purposes. This policy will provide Emergency Medical and Life Insurance coverage should an emergency happen while you are traveling. In accompaniment, there is coverage for security evacuations, as well a travel assistance line which helps with pre-trip planning and finding contracted facilities worldwide.
   * Coverage:
      - Accidental Death [enhanced coverage]: 5 times Annual Salary up to USD 500,000.
      - Out of Country Emergency Medical: Coverage up to $250,000 per occurrence. If there is an injury or sickness while outside of his or her own country that requires treatment by a physician.
      - Security Evacuation with Natural Disaster: If an occurrence takes place outside of his or her home country and Security Evacuation is required, you will be transported to the nearest place of safety.
      - Personal Deviation: Coverage above is extended if personal travel is added on to a business trip. Coverage will be provided for 25% of length of the business trip.
      - Trip Duration: Coverage provided for trips less than 180 days. 
      - Baggage & Personal Effects Benefit: $500 lost bag coverage up to 5 bags.
   * For any assistance with claims or questions, please contact the [People Operations Specialist](https://about.gitlab.com/team/#brittany_rohde).
1. [Further incentives](https://about.gitlab.com/handbook/incentives), such as
   - [Sales Target Dinner Evangelism Reward](https://about.gitlab.com/handbook/incentives/#sales-target-dinner)
   - [Discretionary Bonuses](https://about.gitlab.com/handbook/incentives/#discretionary-bonuses)
   - [Referral Bonuses](https://about.gitlab.com/handbook/incentives/#referral-bonuses)
   - [Work Remotely Travel Grant](https://about.gitlab.com/handbook/incentives/#travel-grant)

## Specific to employees based in the Netherlands
{: #nl-specific-benefits}

Dutch employees get the customary month of vacation money in the month of May.

## Specific to employees based in the UK
{: #uk-specific-benefits}

For UK based employees GitLab provides paid membership of medical insurance with AXA PPP. Family members can be added to the insurance and GitLab will pay 50% of the cost to include them. Please let peopleops know if you would like to join the scheme. Please also note that this is a taxable benefit.


## Specific to India based employees
{: #india-specific-benefits}

All of the benefits listed below are administered and managed by [Lyra Infosystems](http://lyrainfo.com/). As part of the onboarding process Lyra will reach out to team members in their first week to arrange setup and enrollment.  Should you have any questions about these benefits please contact hr@lyrainfo.com or the People Operations Team at GitLab who are also very happy to assist.

### Medical Benefits

 - Group Mediclaim Policy which will take care of hospitalization expenses, the coverage provided is available for the team member, their spouse and up to two children.  Should additional cover be required this will need to be purchased by the team member in the form of an Individual Policy. This can not be purchased under the Group Mediclaim Policy.
 - Group Personal Accident policy including accidental death benefit.

### Meal Vouchers

Zeta Meal Cards are an optional employee purchased benefit. These Meal Cards work like a Debit Card, which can be used at any outlet selling food items and non-alcoholic beverages that accept card payments. If you would like purchase these a deduction from salary will be made each month.

## Specific to US based employees
{: #us-specific-benefits}

US based employees' payroll and benefits are arranged through TriNet. The most up
to date and correct information is always available to employees through the
[TriNet HRPassport portal](https://www.hrpassport.com) and the various contact forms
and numbers listed there. This brief overview is not intended to replace the
documentation in TriNet, but rather to give our GitLabbers and applicants a
quick reference guide.

If you have any questions in regards to your TriNet paycheck log in to TriNet, then go to [How To Read Your Paycheck](https://www.hrpassport.com/Help/Docs/pdf/Readpaycheck_US.pdf).

Carrier ID cards are normally received within 2-3 weeks from when you submit your benefit elections. If you or your medical providers are in need of immediate confirmation of your coverage, please contact the carrier directly.

### Group Medical Coverage
{: #group-medical}

_If you already have current group medical coverage, you may choose to waive or
opt out of TriNet's group health benefits. If you choose to waive health coverage,
you will receive a $300.00 monthly benefit allowance and will still be able to
enroll in optional plans and flexible spending accounts._

#### Medical

TriNet partners with leading carriers, like Aetna, Florida Blue, Blue Shield of
California and Kaiser, to offer a broad range of medical plans, including high
deductible health plans, PPOs, and HMOs. Medical plan options vary by state, and
may also include regional carriers.

##### Transgender Medical Services

Recently, the [United States Department of Health and Human Services](https://www.hhs.gov/) released a [mandate on transgender non-discrimination](http://www.transgendermandate.org/). As part of this mandate, medical carriers were given time to review their current policies and update to reflect the mandate. Expanded language for coverage should be visible in the 2017-10-01 renewal for TriNet’s plans.

Because there is a wide range of services, treatment, and goals for transgender patients, employees are encouraged to contact their carrier directly to have these discussions.

##### Pregnancy & Maternity Care

With medical plans, GitLab offers pregnancy and maternity care. Depending on the plan you selected through TriNet, your coverages may differ for in-network vs out-of-network, visits, and inpatient care. Please contact TriNet at +1 800 638 0461 with any questions about your plan.

#### Dental

TriNet's four dental carriers -Delta Dental, Aetna, Guardian Dental, and MetLife-
offer a high and a low national dental PPO plan. Aetna and Delta Dental also
will make available a DMO plan in many states.

#### Vision

TriNet also offers a high and a low vision plan nationally through two different
carriers: Aetna and Vision Service Plan (VSP). These plan options ensure that
you can choose the best plan and carrier for your individual vision needs.

#### Premiums

The Company pays 100% of premiums for medical, 100% of premiums for dental and
100% of premiums for vision coverage for employee. In addition, the Company
funds 50% of premiums for medical, 50% of premiums for dental and 50% of premiums
for vision coverage for spouse, dependent, and domestic partner. These contribution
amounts (monthly) are capped at:

| Insurance                          | Company Contribution Cap |
| ---------------------------------- | -----------------------: |
| Medical Employee Only              |                  $524.00 |
| Medical Employee + Spouse          |                  $866.50 |
| Medical Employee + Child(ren)      |                  $796.00 |
| Medical Employee + Family          |                $1,055.00 |
| Group Dental Employee Only         |                   $19.80 |
| Group Dental Employee + Spouse     |                   $30.21 |
| Group Dental Employee + Child(ren) |                   $31.20 |
| Group Dental Employee + Family     |                   $41.60 |
| Group Vision Employee Only         |                    $9.17 |
| Group Vision Employee + Spouse     |                   $13.78 |
| Group Vision Employee + Child(ren) |                   $14.42 |
| Group Vision Employee + Family     |                   $20.30 |

---

You are responsible for the remainder of the premium cost, if any.

### Basic Life Insurance and AD&D
{: #basic-life-ins}

TriNet offers company paid basic life and accidental death and dismemberment (AD&D)
plans. The Company pays for basic life insurance coverage valued at $20,000, which
includes an equal amount of AD&D coverage.

### Group Long-Term Disability Insurance
{: #group-LTD}

The Company provides a policy that may replace up to 60% of your salary, up to
a maximum benefit of $12,500 per month, for qualifying disabilities. A waiting
period of 180 days will apply.

### 401k Plan
{: #401k}

The company offers a 401k plan in which you may make voluntary pre-tax contributions
toward your retirement. We do not currently offer matching contributions. See the
[People Operations](https://about.gitlab.com/handbook/people-operations/) page for
details on eligibility and sign up.


### Optional TriNet Plans Available at Employee Expense
{: #trinet-optional}

#### Flexible Spending Account (FSA) Plans
{: #fsa-plans}

FSAs help you pay for eligible out-of-pocket health care and dependent day care expenses
on a pretax basis. You determine your projected expenses for the Plan Year and then
elect to set aside a portion of each paycheck into your FSA.

#### Supplemental Life Insurance
{: #supp-life}

If you want extra protection for yourself and your eligible dependents, you have
the option to elect supplemental life insurance. You may request coverage yourself
for one to six times your annual salary, with a maximum benefit of $2,000,000.
Spousal coverage is also available in increments of $10,000 up to $150,000, and
child coverage for $10,000. Note that amounts above guaranteed issue
($300,000 for you and $30,000 for your spouse) and certain coverage increases
must be approved by the insurance carrier.

#### Supplemental Accidental Death and Dismemberment Insurance
{: #supp-add}

AD&D covers death or dismemberment from an accident only. You may elect supplemental
AD&D coverage in amounts of $25,000, $50,000, $100,000, $250,000, $500,000 or $750,000.

#### Short and Long-Term Disability Insurance
{: #std-ltd}

Disability insurance plans are designed to provide income protection while you recover
from a disability. This coverage not only ensures that you are able to receive some
income while out on disability; it also provides absence management support that helps
facilitate your return to work. TriNet offers several short and long term disability
plan options. Note: There are five states that have state-mandated disability insurance requirements
California, Hawaii, New Jersey, New York and Rhode Island. If you do not live in one of the listed
states it is recommended that you elect Short Term Disability Insurance through TriNet to protect
your financial situation. For more information on State Disability Insurance please contact People Ops.
